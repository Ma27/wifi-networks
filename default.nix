{ pkgs ? import <nixpkgs> { } }:

with pkgs;

stdenv.mkDerivation rec {
  pname = "wifi-networks";
  version = "0.1.0";

  src = ./.;

  installFlags = [ "PREFIX=$(out)" "VERSION=${version}" ];

  dontBuild = true;
  buildInputs = [ python3 git ];

  doCheck = true;
  checkInputs = with python3.pkgs; [ flake8 pylint mypy ];
  checkPhase = ''
    export HOME=$(mktemp -d)
    make check
  '';
}
