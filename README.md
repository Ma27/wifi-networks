# wifi-networks

[![pipeline status](https://gitlab.com/Ma27/wifi-networks/badges/master/pipeline.svg)](https://gitlab.com/Ma27/wifi-networks/commits/master)

Query available wireless networks and available ones with pretty-printed details.

## Usage

The `wifi-networks` script simply requires an interface to scan
using [`iwlist`](https://linux.die.net/man/8/iwlist) (`wlan0`) by default:

```
$ wifi-networks wlp3s0
```

Internaly, a `iwlist {interface-name} scan` will be invoked and prettified. Bash completion for
the interface name to scan are provided as well.

## Installation

The program and its `bash` completions will be installed using `make` to `/usr/local`
by default.

This can be done by invoking `make`:

```
$ sudo make install
```

If a different prefix shall be used for the code, the `PREFIX` parameter can be used:

```
$ make install PREFIX=$HOME
```

For removal the `uninstall` target can be used:

```
$ make uninstall
```

## Completions

Completions for `bash` are shipped by default and located
under `$PREFIX/share/bash-completions/completion/wifi-networks`.

For ZSH it can be loaded using the `bashcompinit` builtin:

```
autoload -U +X bashcompinit && bashcompinit
source /usr/local/bin/share/bash-completions/completion/wifi-networks
```
