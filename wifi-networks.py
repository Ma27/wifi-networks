#! /usr/bin/env python3

from argparse import ArgumentParser
from subprocess import run, PIPE, CalledProcessError
from shutil import which
from sys import exit, stdout, argv
from typing import Optional, List, Dict, Any
from re import sub
import os
import tempfile


DNS_EXTRACT = """#!/bin/sh
if [ "$1" = bound ]; then
    echo "$dns"
fi
"""


def colorize(code: int, content: str) -> str:
    if stdout.isatty():
        return f"\x1b[{code}m{content}\x1b[0m"
    return content


def match_property(name: str, line: str, prefix: int = 20, delim: str = ":") -> Optional[str]:
    prefixStr = "{0}{1}{2}".format(' ' * prefix, name, delim)
    if prefixStr in line:
        return sub(r'^"(.*)"$', r"\1", line.split(prefixStr, 1)[1])
    return None


def gather_wireless_info(output: List[str]) -> List[Dict[str, Any]]:
    networks: List[Dict[str, Any]] = []
    current_object: Dict[str, Any] = {}
    first_found: bool = False
    for line in output:
        if "{0}Cell".format(' ' * 10) in line:
            if first_found:
                networks.append(current_object)
                current_object = {}
                continue
            first_found = True

        ssid = match_property("ESSID", line)
        if ssid is not None:
            current_object["ssid"] = ssid
        freq = match_property("Frequency", line)
        if freq is not None:
            current_object["freq"] = freq.split(" (", 1)[0]
        encr = match_property("Encryption key", line)
        if encr is not None:
            current_object["enc"] = colorize(32, "✔") if encr == "on" else colorize(31, "✗")
        auth_suites = match_property("Authentication Suites (1) ", line, 24)
        if auth_suites is not None:
            current_object["auth"] = colorize(1, auth_suites[1:])
        quality = match_property("Quality", line, 20, "=")
        if quality is not None:
            expression = quality.split("  ", 1)[0]
            left, right = expression.split("/", 1)
            current_object["quality"] = int(int(left) / int(right) * 100)

    if current_object != {}:
        networks.append(current_object)

    return networks


def get_executable_or(name: str, die: bool = True) -> Optional[str]:
    executable = which(name)
    if executable is None:
        if die:
            print(colorize(31, f"Executable `{name}` cannot be found in $PATH!"))
            exit(1)
        return None

    return executable


def parse_dns(interface: str) -> Optional[str]:
    udhcpc = get_executable_or("udhcpc", False)
    with tempfile.NamedTemporaryFile('w+', delete=False) as tmp:
        tmp.write(DNS_EXTRACT)
        os.chmod(tmp.name, 777)
        tmp.close()

        wifi_dns = None
        if udhcpc is None:
            wifi_dns = colorize(31, "Cannot determine current DNS due to a missing udhcpc")
        else:
            with open(os.devnull, 'w+') as nulldescr:
                wifi_dns = run(
                    [
                        udhcpc, "--quit", "--now", "-f",
                        "-i" f"{interface}", "-O", "dns", "--script", tmp.name
                    ],
                    stdout=PIPE,
                    stderr=nulldescr
                ).stdout.decode("utf-8").rstrip("\n")

    os.unlink(tmp.name)

    return wifi_dns


def parse_ghz(input: str) -> float:
    # GHz == Hz / 10^9
    return float(input) / pow(10, 9)


def main() -> None:
    parser = ArgumentParser(description="List available wifi networks", prog=argv[0])
    parser.add_argument("interface", default="wlan0", help="Which interface to query")
    parser.add_argument('--version', action='version', version='%(prog)s git-dev')

    args = parser.parse_args()

    iwlist_exec = get_executable_or('iwlist')
    try:
        output = run(
            [f"{iwlist_exec}", f"{args.interface}", "scan"],
            stdout=PIPE,
            check=True,
            stderr=PIPE
        )
    except CalledProcessError as e:
        print(colorize(31, "Network scan ({0}) failed with exit code {1}!".format(e.cmd, e.returncode)))
        exit(1)

    networks = gather_wireless_info(output.stdout.decode("utf-8").splitlines())
    if not networks:
        print(colorize(31, f"No wireless networks found for interface {colorize(1, args.interface)}"))
        exit(1)

    iwgetid_exec = get_executable_or('iwgetid')
    current_ssid = run(
        [f"{iwgetid_exec}", "-r"],
        stdout=PIPE,
        stderr=PIPE
    ).stdout.decode("utf-8").replace("\n", "")

    current_freq = run(
        [f"{iwgetid_exec}", "-r", "--freq"],
        stdout=PIPE,
        stderr=PIPE
    ).stdout.decode("utf-8").replace("\n", "")

    for n in networks:
        if 'auth' not in n:
            n['auth'] = colorize(31, "None")

        if current_ssid == n['ssid'] and float(n['freq'][:-4]) == parse_ghz(current_freq):
            current_indicator = colorize(1, '*')
            wifi_dns = parse_dns(args.interface)

            if wifi_dns is None:
                wifi_dns = colorize(
                    33,
                    f"Cannot determine dns for {current_ssid}, info not in dhcpcd output"
                )
        else:
            current_indicator = " "
            wifi_dns = " "

        print(f"{current_indicator} {colorize(1, colorize(32, 'Network'))}: "
              + f"{colorize(32, n['ssid'])} ({colorize(1, n['quality'])}%)")
        print(f"    {colorize(1, 'Secured')}: {n['enc']} ({n['auth']})")

        if current_indicator != " ":
            print(f"    {colorize(1, 'DNS')}: {wifi_dns}")

        print(f"    {colorize(1, 'Frequency')}: {n['freq']}%s" % ("\n" if n != networks[-1] else ""))


if __name__ == "__main__":
    main()
