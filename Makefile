PREFIX ?= /usr/local
INSTALL ?= install
FLAKE8 ?= flake8
PYLINT ?= pylint
MYPY ?= mypy
SED ?= sed
VERSION ?= $(shell git describe --tags)

install:
	$(SED) -i -e 's/git-dev/$(VERSION)/' wifi-networks.py
	$(INSTALL) -Dm0755 wifi-networks.py $(PREFIX)/bin/wifi-networks
	$(INSTALL) -Dm0755 bash_completion/wifi-networks $(PREFIX)/share/bash-completion/completions/wifi-networks

uninstall:
	rm $(PREFIX)/bin/wifi-networks
	rm $(PREFIX)/share/bash-completion/completions/wifi-networks

SCRIPTS := $(wildcard *.py)

check:
	$(FLAKE8) $(SCRIPTS) --max-line-length 105
	$(PYLINT) $(SCRIPTS) --max-line-length 105
	$(MYPY) $(SCRIPTS)

.PHONY: install uninstall
